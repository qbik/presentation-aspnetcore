
# .Net Core 
&nbsp;
### what is it and why should I care?  
&nbsp; 

 
jakub.pawlowski@legimi.com
<!--.element: style="font-size: 0.6em; " -->

---

# Agenda

* What is .Net Core
* History and Motivation
* Cross-platform
* Performance
* Why should I care?

---

# Have you ever?

* heard of .Net Core, or Asp.Net Core <!-- .element: class="fragment" -->
 - tried it <!-- .element: class="fragment" -->
* written Asp.Net MVC Application <!-- .element: class="fragment" -->
 - written custom Asp.Net Module <!-- .element: class="fragment" -->
 - written Owin middleware <!-- .element: class="fragment" -->
* used node.js <!-- .element: class="fragment" -->
* written a PCL <!-- .element: class="fragment" -->
* used Mono <!-- .element: class="fragment" -->
* or Xamarin <!-- .element: class="fragment" -->

---

# What is .Net Core


## .Net Framework

![dotnet stack](img/dotnet.png)
<!-- .element: style="height:600px;" -->


## .Net Core

Like .Net Framework, but:
* Cross-Platform
* Modular
* Open Source
* Some APIs are different, some are missing, ie:
 - no Code Access Security
 - no AppDomains

Note:
The major differences between .NET Core and the .NET Framework:
App-models -- .NET Core does not support all the .NET Framework app-models, in part because many of them are built on Windows technologies, such as WPF (built on top of DirectX). The console and ASP.NET Core app-models are supported by both .NET Core and .NET Framework.
APIs -- .NET Core contains many of the same, but fewer, APIs as the .NET Framework, and with a different factoring (assembly names are different; type shape differs in key cases). These differences currently typically require changes to port source to .NET Core. .NET Core implements the .NET Standard Library API, which will grow to include more of the .NET Framework BCL API over time.
Subsystems -- .NET Core implements a subset of the subsystems in the .NET Framework, with the goal of a simpler implementation and programming model. For example, Code Access Security (CAS) is not supported, while reflection is supported.
Platforms -- The .NET Framework supports Windows and Windows Server while .NET Core also supports macOS and Linux.
Open Source -- .NET Core is open source, while a read-only subset of the .NET Framework is open source.

---

# History and Motivation


## It all started with Asp.Net...
* Web Pages (Razor)
* WebAPI
* MVC
* Web Forms 

Notes:
first commit in dotnet/corefx: Nov 8, 2014
first commit in aspnet/Mvc: Dec 12, 2013


![asp.net 4 frameworks](img/aspnet4.png)

Similar, but different: DI, Filters, Routes, Error handling


# Motivation
## Asp.Net vNext

* Unify MVC and WebAPI                                                     <!-- .element: class="fragment" -->
* Remove system.web.dll dependency                                             <!-- .element: class="fragment" -->
* Faster development cycles (not bound to System.Web and whole .Net Framework) <!-- .element: class="fragment" -->
* Why don’t we split everything into small nugets?                             <!-- .element: class="fragment" -->
* Be like node.js:                                                             <!-- .element: class="fragment" -->
  - modular                                                                    <!-- .element: class="fragment" -->
  - start from nothing and build up                                            <!-- .element: class="fragment" -->
* instead of IIS model:                                                        <!-- .element: class="fragment" -->
  - a Swiss knife that has everything                              <!-- .element: class="fragment" -->

Note:
Then it became Asp.Net 5, then Asp.Net Core.

---

# Architecture


# Old Asp.Net stack

![old asp.net stack](img/aspnet-old.png)

Note:
image taken from http://www.slideshare.net/c_horsdal/aspnet-v-next-aanug-20140817


# Asp.Net Core stack

![asp.net Core stack](img/aspnetcore-stack.png)


# Open Source the world

![aspnet open source](img/aspnet-oss.png)

---

# Demo 1

<div>
* installation: http://dot.net
* dotnet new - restore - run 
* Project.json & nuget
* targeting different frameworks
</div>      
<!-- .element: class="fragment" -->

---

# Project.json

* project.json all the things
* why project.json is better (dependency tree, lockfile, etc)
* where is app.config
* binding redirects with project.json

## ...And it's [dead](https://blogs.msdn.microsoft.com/webdev/2016/05/11/notes-from-the-asp-net-community-standup-may-10-2016/) now.
<!-- .element: class="fragment" -->

---

# What's with the naming?


## Naming Confusion

* Asp.net 4.6
* Asp.net vNext -> Asp.net 5 -> __Asp.net Core 1.0.0__
* Kvm, kre -> dnvm, dnx, dnu -> __dotnet__
* .Net Core <-> .Net Framework

Note:
* .Net Foundation
* Asp.Net Core, EF Core
* CoreCLR, CoreFX, CoreRT


## .Net Core on .Net Framework
You can use __dotnet__ and still target .Net Framework

You can use Asp.Net Core on regular .Net Framework


## Alpha, Beta, Gamma...

| version | date  | info |
| :---------- | :--------- |:--------- | 
| v1.0.0-alpha2 | Jul 6 | 2014 __KVM, KRE__ |
| alpha3 | Aug 16, 2014|
| alpha4 |  Oct 6, 2014|
| beta1 | Nov 12, 2014|
| beta2 | Jan 13, 2015|
| beta3 | Mar 12, 2014 | __change to DNVM, DNU, DNX__|
| beta4 | May 1, 2015|
| beta5 | Jul 1, 2015|
| beta6 | Jul 28, 2015|
| beta7 | Sep 2, 2015|
| beta8 | Oct 14, 2015|
| rc1-final | Nov 18, 2015|
| rc1-update1 |  Dec 1, 2015 | __“we are Stable”__|
| rc2 | Aspnet Core | __“no, we weren't stable...”: dotnet cli__|
| 1.0.0 | June 27, 2016|
<!-- .element: style="font-size: 0.6em" -->

---

# Toolchain


## Removing the magic

* Everything is commandline
* One __dotnet__ to rule them all
 - extensibility model similar to git
* Use your favourite editor
  - Intellisense through 3rd party tools, like Omnisharp
* Using popular and trusted tools for Web – gulp, grunt, less, etc.

---

# Demo 2

<div>
### Tools

* asp.net core app
* Vs code
* Grunt, gulp, etc
* dotnet test
</div>
<!-- .element: class="fragment" -->

---

# Cross platform


## .Net Core

Runs on Windows, Linux, Mac 

How about Android and iOS?
<!-- .element: class="fragment" -->


## .NET Standard Library

> The .NET Standard Library is a formal specification of .NET APIs that are intended to be available on all .NET runtimes.
[https://docs.microsoft.com/pl-pl/dotnet/articles/standard/library](https://docs.microsoft.com/pl-pl/dotnet/articles/standard/library)<!-- .element: style="font-size:0.5em" -->



| Platform | Alias |  |  |  |  |  | | |
| :---------- | :--------- |:--------- |:--------- |:--------- |:--------- |:--------- |:--------- |:--------- |
|.NET Standard | netstandard | 1.0 | 1.1 | 1.2 | 1.3 | 1.4 | 1.5 | 1.6 |
|.NET Core|netcoreapp|&rarr;|&rarr;|&rarr;|&rarr;|&rarr;|&rarr;|1.0|
|.NET Framework|net|&rarr;|4.5|4.5.1|4.6|4.6.1|4.6.2|vNext|
|Mono/Xamarin Platforms||&rarr;|&rarr;|&rarr;|&rarr;|&rarr;|&rarr;|*|
|Universal Windows Platform|uap|&rarr;|&rarr;|&rarr;|&rarr;|10.0|||
|Windows|win|&rarr;|8.0|8.1|||||
|Windows Phone|wpa|&rarr;|&rarr;|8.1|||||
|Windows Phone Silverlight|wp|8.0|||||||
<!-- .element: style="font-size:0.8em" -->

---

# Performance


## Techempower benchmarks

<p class="caption">
![techempower-r12](img/techempower-r12-plaintext.png)
<small class="caption">Round 12, Plain-Text</small>
<aside class="notes">some notes</aside>
</p>


## Asp.Net 4.6

<p class="caption">
![techempower-r8](img/techempower-r8-plaintext.png)
<small class="caption">Round 8, Plain-Text</small>
<aside class="notes">some notes</aside>
</p>


## Asp.Net Core

![chart1](img/perfchart.jpg)


## Asp.Net Core 

<p class="caption">
![chart1](img/chart1.png)
<br/>
<small class="caption">Round 8, Plain-Text</small>
<aside class="notes">some notes</aside>
</p>

<aside class="notes">
Techempower tests
ASP.NET Core – Exceeds 1.15 Million request/s, 12.6 Gbps
1.15 Million represents a 2300% gain from ASP.NET 4.6!
ASP.NET - 

plain text

old perf lab
asp.net core 1.174M
netty 2.808M

new perf lab (rtm)
asp.net core 5M 
netty 8M 

http://web.ageofascent.com/asp-net-core-exeeds-1-15-million-requests-12-6-gbps/
https://github.com/aspnet/benchmarks#plain-text
</aside>

---

# Why should I care?

* Performace
* Smaller memory footprint
* Cross-Platform 
* Containers (docker)!
* Asp.Net Core middleware pipeline (like Owin)
* Production-ready


# Interoperabiliy

* If it compiles for .Net Core, it will compile for .Net framework (probably)
* NetFx -> dotnet (through nuget)
* dotnet -> NetFx (through nuget, only if you target NetFx)
* Owin middleware - similar, but different
 - it's possible to do with some #ifdefs 


# When to switch

* migrating existing code can be hard and tedious..

* new Asp projects 
* new console apps 
* portable libraries (i.e. for Xamarin) 

---

# Thank you!
## Questions?

---

# Demo 3

<div>
* Simple asp.net site
* Hosting models
* Deploying to IIS
* Debugging
</div>
<!-- .element: class="fragment" -->

---

# Resources 
<div>
* http://dot.net
* http://github/aspnet/home
* http://github/dotnet/home
* http://web.ageofascent.com/asp-net-core-exeeds-1-15-million-requests-12-6-gbps/
* https://www.codeschool.com/courses/try-asp-net-core
* https://github.com/aspnet/benchmarks
* https://www.techempower.com/benchmarks/
* http://www.hanselman.com/blog/BenchmarkingNETCode.aspx
* https://github.com/dotnet/coreclr/issues/2430#issuecomment-166110993
* https://recordnotfound.com/benchmarks-aspnet-8563
* http://www.socalcto.com/2016/07/techempower-benchmarks-and-microsoft.html
* http://www.socalcto.com/2016/07/techempower-benchmarks-and-microsoft.html
* http://www.hanselman.com/blog/SelfcontainedNETCoreApplications.aspx
</div>
<!-- .element style="font-size: 0.5em" -->

---

# Misc


# Edit and Continue

* Roslyn
* dotnet watch


# Deployment

* Hosting in production
* Publishing to IIS

Note:
# Old Asp.Net stack
![asp.net stack](img/aspnet-stack.png)
# Asp.Net 5 stack
![asp.net 5 stack](img/aspnet5-stack.png)
about author on start
netstandard
https://github.com/dotnet/corefx/edit/master/Documentation/architecture/net-platform-standard.md

https://docs.microsoft.com/pl-pl/dotnet/articles/standard/library
https://docs.microsoft.com/pl-pl/dotnet/articles/core/packages

---

# Asp.Net Core


# Asp.Net Core and the modern web

* Totally modular
* Seamless transition from on-premises to cloud
* Open source (accepts contributions)
* Faster development cycles
* Choose your editors and tools
* Cross-platform
* Fast


# Asp.Net Core features

* Hosting
  * Kestrel, Startup
* Middleware
  * Routing, authentication, static files, diagnostics, error handling, session, CORS, localization, custom
* Dependency Injection
* Configuration
* Logging
* Application frameworks
  * MVC, Identiy, SignalR (future)


# Configuration

* environment as a first class citizen
* new configuration model 
* configured user secrets


# ASP.NET Core MVC

* One set of concepts – remove duplication
* Web UI and Web APIs
* Built on ASP.NET Core
* Supports .NET Core
* Runs on IIS or self-hosted
* Deep integration with DI
* *NEW* Tag Helpers


# How do you like your ~~steak~~ Asp.Net?

![](img/steak.png)
