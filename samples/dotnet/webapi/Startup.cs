﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Builder;

namespace WebApi
{
    public class Startup
    {
        public Startup()
        {
            Console.WriteLine("startup ctor");
            // Set up configuration sources.
            // var builder = new ConfigurationBuilder()
            //     .AddJsonFile("appsettings.json");

            // if (env.IsEnvironment("Development"))
            // {
            //     // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
            //     builder.AddApplicationInsightsSettings(developerMode: true);
            // }

            // builder.AddEnvironmentVariables();
            // Configuration = builder.Build().ReloadOnChanged("appsettings.json");
        }

        public IConfigurationRoot Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            // services.AddApplicationInsightsTelemetry(Configuration);
            Console.WriteLine("configuring services");
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app)
        {
            Console.WriteLine("configuring app");
            // loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            // loggerFactory.AddDebug();

            // app.UseIISPlatformHandler();

            // app.UseApplicationInsightsRequestTelemetry();

            // app.UseApplicationInsightsExceptionTelemetry();

            // app.UseStaticFiles();

            app.UseMvc();
        }

        // Entry point for the application.
        //public static void Main(string[] args) => WebApplication.Run<Startup>(args);
        public static void Main(string[] args)
        {
            var hostcfg = new WebHostBuilder()
                // We set the server by name before default args so that command line arguments can override it.
                // This is used to allow deployers to choose the server for testing.
                .UseServer("Microsoft.AspNetCore.Server.Kestrel")
                .UseDefaultHostingConfiguration(args)
                .UseIISIntegration()
                .UseStartup<Startup>();
                
            System.Console.WriteLine("building webhost");
            var host = hostcfg.Build();

            System.Console.WriteLine("running webhost");
            host.Run();
            
            System.Console.WriteLine("done");
        }
    }
}
