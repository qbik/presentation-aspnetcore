﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(aspnet_helloworld.Startup))]

namespace aspnet_helloworld
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Use(async (ctx, next) => {
                var msg = "Hello world!";
                var bytes = System.Text.Encoding.UTF8.GetBytes(msg);
                ctx.Response.Body.Write(bytes, 0, bytes.Length);
                //await next();
            });
        }
    }
}
