﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

namespace perftest1
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {       
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            //loggerFactory.AddConsole(LogLevel.Debug);
            app.Use(async (ctx, next) => {
                var msg = "Hello world!";
                var bytes = System.Text.Encoding.UTF8.GetBytes(msg);
                ctx.Response.Body.Write(bytes, 0, bytes.Length);
                //await next();
            });
        }
    }
}
