﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.Extensions.DependencyInjection;
using System.Collections;

namespace CommandLineSample
{
    public class Program
    {
        public Program(IServiceProvider prov)
        {
            Console.WriteLine("Program ctor");
            ListEntries(prov);
        }

        private void ListEntries(IServiceProvider prov)
        {
            System.Console.WriteLine($"registered services:");

            var fields = prov.GetType().GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            foreach (var f in fields)
            {
                Console.WriteLine($"{f.Name}");
            }

            var entries = fields.Single(f => f.Name == "_entries").GetValue(prov);
            foreach (var e in (entries as IEnumerable))
            {
                Console.WriteLine($"{e}");
            }
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            Console.WriteLine("ConfigureServices");
            return services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            Console.WriteLine("Configure");
        }


        // Entry point for the application.
        public static void Main(string[] args)
        {
            Console.WriteLine("static Main");
        }
    }
}
