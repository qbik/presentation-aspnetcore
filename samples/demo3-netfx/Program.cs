﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            #if NETFX
            
            var log = log4net.LogManager.GetLogger("root");
            log.Debug("hello!");
            #endif
        }
    }
}
